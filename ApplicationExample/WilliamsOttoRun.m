%% Williams Otto Process
% written by Christoph D. Pahlke
% last changes: 31.03.2017
%
% This file contains the surrounding MATLAB skript corresponding to the
% three GAMS models WilliamsOttoNPW.gms, WilliamsOttoPBP.gms and 
% WilliamsOttoNPWfix.gms. It belongs to the research project "How to handle
% GAMS Models via MATLAB", written by Christoph D. Pahlke at Max Planck
% Institute Magdeburg
%
% Parametrization and initialization with MATLAB
% using wgdx()/rgdx()-functions of the GAMS-MATLAB-interface
%
% for running this file, the GAMS interface must be known to MATLAB
% i.e. the GAMS-directory (which contains the files wgdx.m, rgdx.m) must be
% added to the MATLAB path
%
% This file executes the following files for creating the plots:
% plotResults_NPW_PBP.m, plotResults_NPW_PBP_TradeOff.m,
% plotResults_NPW_PBP_eta_T_V.m, plotResults_NPW_PBP_FA_FB.m

clear; close all; clc;

%% some local initializations
%number of variations for finding Pareto front
NPWstepNumber=400;
%GDX-file into which the data will be written
dataGDX='data';
%GDX-file into which gams will write the solution
solGDX='solution';
%GMS-file which contains the GAMS model for maximizing NPW
modelGMS_NPW='WilliamsOttoNPW';
%GAMS model for minimizing PBP
modelGMS_PBP='WilliamsOttoPBP';
%GAMS model for epsilon-constraint method
modelGMS_NPWfix='WilliamsOttoNPWfix';


%% raw model data
%model parameters
k_0=[5.9755e9 2.5962e12 9.6283e15];
E_A=[12000 15000 20000]; % Degree Rankine
%conversion to Kelvin is done in GAMS model by division by 1.8
rho=800.92317; % kg/m^3 (=50 lbs/ft^3)

F_P=2160; % kg/h, Product stream

%initial values
eta_init=0.3;
F_R_init=[8239 27594 1509 27426 1635]; % kg/h
T_init=350;%K
F_R_init=[F_R_init 0.1*F_R_init(4)+F_P];% kg/h
F_A_init=6136; % kg/h

%% set structures (indices)
alphas.name = 'alpha';
alphas.ts   = '"components"';
alphas.type = 'set';
alphas.uels = {'A', 'B', 'C', 'E', 'G', 'P'};

ms.name = 'm';
ms.ts   = 'reaction index';
ms.type = 'set';
ms.uels = {1:3};

%% data structures
%prepare the model data for wgdx()-function
k_0s.name = 'k_0';
k_0s.ts   = '"reaction constants"';
k_0s.type = 'parameter';
k_0s.val = k_0;
k_0s.form = 'full';
k_0s.dim = 1;
k_0s.uels = ms.uels;

E_As.name = 'E_A';
E_As.ts   = '"activation energy"';
E_As.type = 'parameter';
E_As.val = E_A;
E_As.form = 'full';
E_As.dim = 1;
E_As.uels = ms.uels;

rhos.name = 'rho';
rhos.ts   = '"density"';
rhos.type = 'parameter';
rhos.val = rho;
rhos.form = 'full';
rhos.dim = 0;

F_Ps.name = 'F_P';
F_Ps.ts   = '"product stream"';
F_Ps.type = 'parameter';
F_Ps.val = F_P;
F_Ps.form = 'full';
F_Ps.dim = 0;

eta_inits.name = 'eta_init';
eta_inits.ts   = '"initial purge split ratio"';
eta_inits.type = 'parameter';
eta_inits.val = eta_init;
eta_inits.form = 'full';
eta_inits.dim = 0;

F_R_inits.name = 'F_R_init';
F_R_inits.ts   = '"initial CSTR mass streams"';
F_R_inits.type = 'parameter';
F_R_inits.val = F_R_init;
F_R_inits.form = 'full';
F_R_inits.dim = 1;
F_R_inits.uels = alphas.uels;

T_inits.name = 'T_init';
T_inits.ts   = '"initial rector Temperature"';
T_inits.type = 'parameter';
T_inits.val = T_init;
T_inits.form = 'full';
T_inits.dim = 0;

F_A_inits.name = 'F_A_init';
F_A_inits.ts   = '"initial flux of A"';
F_A_inits.type = 'parameter';
F_A_inits.val = F_A_init;
F_A_inits.form = 'full';
F_A_inits.dim = 0;

%% write the data into a gdx-file
if exist([dataGDX '.gdx'],'file')
  delete ([dataGDX '.gdx']);
end
wgdx(dataGDX,alphas,ms,...
    k_0s,E_As,rhos,F_Ps,eta_inits,F_R_inits,T_inits,F_A_inits);
% %For saving output of gdxInfo to a txt-file do:
% if exist([dataGDX '.txt'],'file'), delete([dataGDX '.txt']);end;
% diary([dataGDX '.txt']);
% gdxInfo(dataGDX);
% diary off

%% first run of the model: maximize NPW
%delete possibly existing old solution file
if exist([solGDX '.gdx'],'file')
  delete ([solGDX '.gdx']);
end
fprintf('First run of the model: minimize NPW\n');
tic
gams(modelGMS_NPW); %run the model
toc

% get the results from GDX-file produced by GAMS
rs = struct('name','modelStat','form','full');
r=rgdx(solGDX,rs);
modelStat_1=r.val;

rs = struct('name','solveStat','form','full');
r=rgdx(solGDX,rs);
solveStat_1=r.val;

rs = struct('name','PBPLevel','form','full');
r=rgdx(solGDX,rs);
PBP_1=r.val;

rs = struct('name','NPWLevel','form','full');
r=rgdx(solGDX,rs);
NPW_1=r.val;

rs = struct('name','VLevel','form','full');
r=rgdx(solGDX,rs);
V_1=r.val;

rs = struct('name','TLevel','form','full');
r=rgdx(solGDX,rs);
T_1=r.val;

rs = struct('name','etaLevel','form','full');
r=rgdx(solGDX,rs);
eta_1=r.val;

rs = struct('name','F_ALevel','form','full');
r=rgdx(solGDX,rs);
F_A_1=r.val;

rs = struct('name','F_BLevel','form','full');
r=rgdx(solGDX,rs);
F_B_1=r.val;

rs = struct('name','F_purgeLevel','form','full');
r=rgdx(solGDX,rs);
F_purge_1=r.val;

% Show results 
fprintf('modelstat = %d (1,2 = optimal solution)\n', modelStat_1);
fprintf('solvestat = %d (1 = normal completion)\n', solveStat_1);
fprintf('PBP_1 = %.2f\n',PBP_1);
fprintf('NPW_1 =  $%.3e\n\n\n',NPW_1);

%% Second run of the model: minimize PBP
%delete possibly existing old solution file
if exist([solGDX '.gdx'],'file')
  delete ([solGDX '.gdx']);
end
fprintf('Second run of the model: minimize PBP\n')
tic
gams(modelGMS_PBP); %run the model
toc

% get the results from GDX-file produced by GAMS
rs = struct('name','modelStat','form','full');
r=rgdx(solGDX,rs);
modelStat_2=r.val;

rs = struct('name','solveStat','form','full');
r=rgdx(solGDX,rs);
solveStat_2=r.val;

rs = struct('name','PBPLevel','form','full');
r=rgdx(solGDX,rs);
PBP_2=r.val;

rs = struct('name','NPWLevel','form','full');
r=rgdx(solGDX,rs);
NPW_2=r.val;

rs = struct('name','VLevel','form','full');
r=rgdx(solGDX,rs);
V_2=r.val;

rs = struct('name','TLevel','form','full');
r=rgdx(solGDX,rs);
T_2=r.val;

rs = struct('name','etaLevel','form','full');
r=rgdx(solGDX,rs);
eta_2=r.val;

rs = struct('name','F_ALevel','form','full');
r=rgdx(solGDX,rs);
F_A_2=r.val;

rs = struct('name','F_BLevel','form','full');
r=rgdx(solGDX,rs);
F_B_2=r.val;

rs = struct('name','F_purgeLevel','form','full');
r=rgdx(solGDX,rs);
F_purge_2=r.val;

% Show results 
fprintf('modelstat = %d (1,2 = optimal solution)\n', modelStat_2);
fprintf('solvestat = %d (1 = normal completion)\n', solveStat_2);
fprintf('PBP_2 = %.2f\n',PBP_2);
fprintf('NPW_2 =  $%.3e\n\n\n',NPW_2);

%% Third run of the model: epsilon constraint method
% (NPW fixed for finding pareto front)

%Start point: minimal PBP and corresponding NPW
modelStat_start=modelStat_2;
solveStat_start=solveStat_2;
NPW_start=NPW_2; 
PBP_start=PBP_2;
V_start=V_2;
T_start=T_2;
eta_start=eta_2;
F_A_start=F_A_2;
F_B_start=F_B_2;
F_purge_start=F_purge_2;

%end point: maximal NPW and corresponding PBP
modelStat_end=modelStat_1;
solveStat_end=solveStat_1;
NPW_end=NPW_1;
PBP_end=PBP_1;
V_end=V_1;
T_end=T_1;
eta_end=eta_1;
F_A_end=F_A_1;
F_B_end=F_B_1;
F_purge_end=F_purge_1;
NPW_step=(NPW_end-NPW_start)/NPWstepNumber; %calculate step length
NPW_for=[NPW_start+NPW_step:NPW_step:NPW_end-NPW_step];

modelStat_for=zeros(size(NPW_for));
solveStat_for=zeros(size(NPW_for));
PBP_for=zeros(size(NPW_for));
V_for=zeros(size(NPW_for));
T_for=zeros(size(NPW_for));
eta_for=zeros(size(NPW_for));
F_A_for=zeros(size(NPW_for));
F_B_for=zeros(size(NPW_for));
F_purge_for=zeros(size(NPW_for));

NPWfixs.name = 'NPWfix';
NPWfixs.ts   = '"fixed NPW for finding pareto front"';
NPWfixs.type = 'parameter';
NPWfixs.form = 'full';
NPWfixs.dim = 0;

fprintf('Third run of the model: eps-constraint method\n')
tic
fprintf('Progress: %05.2f %%',1/length(NPW_for)*100);
for i=1:length(NPW_for)
    fprintf('\b\b\b\b\b\b\b%05.2f %%',i/length(NPW_for)*100)
    NPWfixs.val = NPW_for(i);

    
    if exist([dataGDX '.gdx'],'file')
        delete ([dataGDX '.gdx']);
    end
    wgdx(dataGDX,alphas,ms,...
        k_0s,E_As,rhos,F_Ps,eta_inits,F_R_inits,T_inits,F_A_inits,NPWfixs);
    
    %% run the model with new parameter
    %delete possibly existing old solution file
    if exist([solGDX '.gdx'],'file')
        delete ([solGDX '.gdx']);
    end
    gams(modelGMS_NPWfix); %run the model
    
    %% read data
    rs = struct('name','PBPLevel','form','full');
    r=rgdx(solGDX,rs);
    PBP_for(i)=r.val;
    
    rs = struct('name','modelStat','form','full');
    r=rgdx(solGDX,rs);
    modelStat_for(i)=r.val;

    rs = struct('name','solveStat','form','full');
    r=rgdx(solGDX,rs);
    solveStat_for(i)=r.val;
    
    rs = struct('name','VLevel','form','full');
    r=rgdx(solGDX,rs);
    V_for(i)=r.val;
    
    rs = struct('name','TLevel','form','full');
    r=rgdx(solGDX,rs);
    T_for(i)=r.val;
    
    rs = struct('name','etaLevel','form','full');
    r=rgdx(solGDX,rs);
    eta_for(i)=r.val;
    
    rs = struct('name','F_ALevel','form','full');
    r=rgdx(solGDX,rs);
    F_A_for(i)=r.val;
    
    rs = struct('name','F_BLevel','form','full');
    r=rgdx(solGDX,rs);
    F_B_for(i)=r.val;
    
    rs = struct('name','F_purgeLevel','form','full');
    r=rgdx(solGDX,rs);
    F_purge_for(i)=r.val;
end
fprintf('\n');
toc

%create complete data array for post processing plotting
NPW_for=[NPW_start NPW_for NPW_end];
PBP_for=[PBP_start PBP_for PBP_end];
V_for=[V_start V_for V_end];
T_for=[T_start T_for T_end];
eta_for=[eta_start eta_for eta_end];
F_A_for=[F_A_start F_A_for F_A_end];
F_B_for=[F_B_start F_B_for F_B_end];
F_purge_for=[F_purge_start F_purge_for F_purge_end];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Average Gradient Tradeoff (agt)
avgGrad=(PBP_for(end)-PBP_for(1))/(NPW_for(end)-NPW_for(1)); %average gradient of Pareto front
dPBP=diff(PBP_for)./diff(NPW_for); %gradient at each point of Pareto front
[~,avgGrad_i]=min(abs(dPBP-avgGrad)); %find position of avg grad on the front

%% output table
results=[V_for(1)       V_for(avgGrad_i)        V_for(end);...
         T_for(1)       T_for(avgGrad_i)        T_for(end);...
         eta_for(1)     eta_for(avgGrad_i)      eta_for(end);...
         F_A_for(1)     F_A_for(avgGrad_i)      F_A_for(end);...
         F_B_for(1)     F_B_for(avgGrad_i)      F_B_for(end);...
         PBP_for(1)     PBP_for(avgGrad_i)      PBP_for(end);...
         NPW_for(1)     NPW_for(avgGrad_i)      NPW_for(end)];
printmat(results,'Results','V T eta F_A F_B PBP NPW','minPBP avgGrad maxNPW')

%% plot Results
fprintf('\nFigures are plotted and saved to the disk.\n');
fprintf('Cropping is done if a latex distribution is installed.\n\n');
plotResults_NPW_PBP;

%% plot further results
plotResults_NPW_PBP_TradeOff;
plotResults_NPW_PBP_eta_T_V;
plotResults_NPW_PBP_FA_FB;










