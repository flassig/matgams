readme.txt

Instruction file for the source code of the application example
belonging to the research project "How to handle GAMS Models via MATLAB"

----------This directory should contain the following 8 files:-------------
WilliamsOttoRun.m
plotResults_NPW_PBP.m
plotResults_NPW_PBP_TradeOff.m
plotResults_NPW_PBP_eta_T_V.m
plotResults_NPW_PBP_FA_FB.m
WilliamsOttoNPW.gms
WilliamsOttoPBP.gms 
WilliamsOttoNPWfix.gms

-----------Purpose of the files:-------------------------------------------
WilliamsOttoRun.m:
This is the main MATLAB script and the file to be executed.

plotResults_*.m
These files contain the the graphical output

WilliamsOtto*.gms
These files contain the GAMS models.