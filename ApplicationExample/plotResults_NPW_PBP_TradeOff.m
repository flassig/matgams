%% create plot of the Pareto front and find a tradeoff
% written by Christoph D. Pahlke
% last changes: 31.03.2017
%
% is automatically executed by 'WilliamsOttoRun.m'
%
% can only be executed if the necessary variables created by
% 'WilliamsOttoRun.m' are stored in the MATLAB Workspace
%
% needs a latex distribution for cropping pdf figures

%% Begin of plotting
x=NPW_for/1e6;
y=PBP_for;

xMin=3.7;
xMax=7.5;
yMin=0.9;
yMax=2.1;

hfig=figure();
hold on;
grid on;
set(hfig,'defaultTextInterpreter','latex');
set(hfig,'defaultAxesFontSize',10);
set(hfig,'defaultTextFontSize',10);

xlabel('$\mathit{NPW}$ [$10^6$~\$]')
ylabel('$\mathit{PBP}$ [a]')
xlim([xMin xMax]);
ylim([yMin yMax]);

%title('Pareto Front max(NPW) vs. min(PBP)')
%legend('','')

plot(x,y)

plot(x(end),y(end),'ko')
text(x(end),y(end),'maximal $\mathit{NPW}$~~','HorizontalAlignment','right')
plot(x(1),y(1),'ko')
text(x(1),y(1),' minimal $\mathit{PBP}$','VerticalAlignment','top')


% Average Gradient Tradeoff (agt)
plot(x(avgGrad_i),y(avgGrad_i),'ro')
text(x(avgGrad_i),y(avgGrad_i),...
    {'~~average gradient ' '~~tradeoff '},...
    'HorizontalAlignment','left','VerticalAlignment','top','Color','r');

agtX=x(avgGrad_i);
agtY=y(avgGrad_i);
deltaX=0.75;
tangentX=[agtX-deltaX agtX+deltaX];
tangentY=[agtY-avgGrad*1e6*deltaX agtY+avgGrad*1e6*deltaX];
plot(tangentX,tangentY,'k--') %plot tangent to Pareto front
plot([x(1) x(end)],[y(1), y(end)],'k--')


% % Utopia tradeoff
% % the result depend on the scaling. That is because it is a comparison of
% % years and million dollar. So some scaling factors are needed. 
% plot(NPW_end,PBP_start,'ko')
% text(NPW_end,PBP_start,'Utopia~~','HorizontalAlignment','right')
% UtopiaVectors=[NPW_for./1e6-NPW_end./1e6;PBP_for-PBP_start];
% UtopiaNorms=zeros(1,length(UtopiaVectors));
% % calculate the distance of each Pareto front point from Utopia Point
% for i=1:length(UtopiaVectors) 
%     UtopiaNorms(i)=norm(UtopiaVectors(:,i));
% end
% [~,utopia_i]=min(UtopiaNorms);
% plot(NPW_for(utopia_i),PBP_for(utopia_i),'go')
% text(NPW_for(utopia_i),PBP_for(utopia_i),...
%     'utopia tradeoff~~~~','HorizontalAlignment','right','Color','g');


%% End of plotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Printing/saving
%% define size
breite=14;
hoehe=10;
links=0;
unten=0;

set(hfig,'paperunits','centimeters')
set(hfig, 'PaperPositionMode', 'manual');
set(hfig,'PaperSize',[breite,hoehe])
set(hfig,'PaperPosition',[links,unten,breite,hoehe])

%% save plot
%[~, fileName, ~]=fileparts(mfilename('fullpath'));
fileName='Pareto_NPWvsPBP_tradeoff';
saveas(hfig,[fileName '.fig'])
%print(hfig,'-dpng',[fileName '.png'])
print(hfig,'-dpdf',[fileName '.pdf'])

%crop the margins around the plot with latex-tool pdfcrop
%works only if pdfcrop is on the system path
system(['pdfcrop -margins 10 ' fileName '.pdf ' fileName '.pdf']);