%% create plot of the Pareto front
% written by Christoph D. Pahlke
% last changes: 31.03.2017
%
% is automatically executed by 'WilliamsOttoRun.m'
%
% can only be executed if the necessary variables created by
% 'WilliamsOttoRun.m' are stored in the MATLAB Workspace
%
% needs a latex distribution for cropping pdf figures

%% Begin of plotting
x=NPW_for/1e6;
y=PBP_for;

xMin=3.7;
xMax=7.5;
yMin=0.9;
yMax=2.1;

hfig=figure();
hold on;
grid on;
set(hfig,'defaultTextInterpreter','latex');
set(hfig,'defaultAxesFontSize',10);
set(hfig,'defaultTextFontSize',10);

xlabel('$\mathit{NPW}$ [$10^6$~\$]')
ylabel('$\mathit{PBP}$ [a]')
xlim([xMin xMax]);
ylim([yMin yMax]);

%title('Pareto Front max(NPW) vs. min(PBP)')
%legend('','')

plot(x,y)

plot(x(end),y(end),'ko')
text(x(end),y(end),'maximal $\mathit{NPW}$~~','HorizontalAlignment','right')
plot(x(1),y(1),'ko')
text(x(1),y(1),' minimal $\mathit{PBP}$','VerticalAlignment','top')


%% End of plotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Printing/saving
%% define size
breite=14;
hoehe=10;
links=0;
unten=0;

set(hfig,'paperunits','centimeters')
set(hfig, 'PaperPositionMode', 'manual');
set(hfig,'PaperSize',[breite,hoehe])
set(hfig,'PaperPosition',[links,unten,breite,hoehe])

%% save plot
%[~, fileName, ~]=fileparts(mfilename('fullpath'));
fileName='Pareto_NPWvsPBP';
saveas(hfig,[fileName '.fig'])
%print(hfig,'-dpng',[fileName '.png'])
print(hfig,'-dpdf',[fileName '.pdf'])

%crop the margins around the plot with latex-tool pdfcrop
%works only if pdfcrop is on the system path
system(['pdfcrop -margins 5 ' fileName '.pdf ' fileName '.pdf']);