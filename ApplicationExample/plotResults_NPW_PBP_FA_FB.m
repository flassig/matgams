%% create subplots with linked x-axes
% written by Christoph D. Pahlke
% last changes: 27.03.2017
%
% is automatically executed by 'WilliamsOttoRun.m'
%
% can only be executed if the necessary variables created by
% 'WilliamsOttoRun.m' are stored in the MATLAB Workspace
%
% needs a latex distribution for cropping pdf figures

NOP=3; %NumberOfPlots
x = [NPW_for/1e6;...
     NPW_for/1e6;...
     NPW_for/1e6];

y = [PBP_for;...
     F_A_for/1e3;...
     F_B_for/1e3];

xLabels={[] [] '$\mathit{NPW}$ [$10^6$~\$]'};
yLabels={'$\mathit{PBP}$ [a]' '$F_A$ [$10^3$~kg/h]' '$F_B$ [$10^3$~kg/h]'};
 
xMin=min(x')-(max(x')-min(x'))/30;
xMax=max(x')+(max(x')-min(x'))/30;

yMin=min(y')-(max(y')-min(y'))/15;
yMax=max(y')+(max(y')-min(y'))/15;

%% local initializations
subi=0; %subplot counter
subArray=[]; %Array of subplots
hfig=figure();
set(hfig,'defaultTextInterpreter','latex');
set(hfig,'defaultAxesFontSize',10);
set(hfig,'defaultTextFontSize',10);


%% subplot1 
subi=subi+1;
subArray=[subArray subplot(NOP,1,subi)];
hold on;
grid on;
%title('Pareto Front max(NPW) vs. min(PBP)')
xlabel(xLabels(subi));
ylabel(yLabels(subi))
xlim([xMin(subi) xMax(subi)]);
ylim([yMin(subi) yMax(subi)]);

plot(x(subi,:),y(subi,:));
% add additional things to plot after here
plot(x(subi,end),y(subi,end),'ko')
text(x(subi,end),y(subi,end),...
    'maximal $\mathit{NPW}$ ','HorizontalAlignment','right')
plot(x(subi,1),y(subi,1),'ko')
text(x(subi,1),y(subi,1),...
    {' minimal $\mathit{PBP}$',''},'VerticalAlignment','middle')
plot(x(subi,avgGrad_i),y(subi,avgGrad_i),'ro')
text(x(subi,avgGrad_i),y(subi,avgGrad_i),...
    {'~~average gradient ' '~~tradeoff '},...
    'HorizontalAlignment','left','VerticalAlignment','top','Color','r');

%% subplot2
subi=subi+1;
subArray=[subArray subplot(NOP,1,subi)];
hold on;
grid on;
%title('Pareto Front max(NPW) vs. min(PBP)')
xlabel(xLabels(subi));
ylabel(yLabels(subi))
xlim([xMin(subi) xMax(subi)]);
ylim([yMin(subi) yMax(subi)]);

plot(x(subi,:),y(subi,:));
% add additional things to plot after here
plot(x(subi,end),y(subi,end),'ko')
plot(x(subi,1),y(subi,1),'ko')
plot(x(subi,avgGrad_i),y(subi,avgGrad_i),'ro')

%% subplot3
subi=subi+1;
subArray=[subArray subplot(NOP,1,subi)];
hold on;
grid on;
%title('Pareto Front max(NPW) vs. min(PBP)')
xlabel(xLabels(subi));
ylabel(yLabels(subi))
xlim([xMin(subi) xMax(subi)]);
ylim([yMin(subi) yMax(subi)]);

plot(x(subi,:),y(subi,:));
% add additional things to plot after here
plot(x(subi,end),y(subi,end),'ko')
plot(x(subi,1),y(subi,1),'ko')
plot(x(subi,avgGrad_i),y(subi,avgGrad_i),'ro')

%% post plotting
linkaxes(subArray,'x');

%% End of plotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Printing/saving
%% define size for print
breite=17;
hoehe=19.9;
links=0;
unten=0;

set(hfig,'paperunits','centimeters')
set(hfig, 'PaperPositionMode', 'manual');
set(hfig,'PaperSize',[breite,hoehe])
set(hfig,'PaperPosition',[links,unten,breite,hoehe])

%% save plot
%[~, fileName, ~]=fileparts(mfilename('fullpath'));
fileName='Pareto_NPWvsPBPandFA_FB';
saveas(hfig,[fileName '.fig'])
%print(hfig,'-dpng',[fileName '.png'])
print(hfig,'-dpdf',[fileName '.pdf'])

%crop the margins around the plot with latex-tool pdfcrop
%works only if pdfcrop is on the system path
system(['pdfcrop -margins 5 ' fileName '.pdf ' fileName '.pdf']);