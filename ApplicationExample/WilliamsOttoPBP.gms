$title Williams Otto Process solved with GAMS

$oneolcom
$onempty
option nlp=conopt4; !!snopt, conopt, minos, knitro, ipopt

SETS
  alpha "components"
  m "reaction index"
;

PARAMETERS
  k_0(m)            "reaction constants"
  E_A(m)            "Energy of activation"
  rho               "density of material flow"
  F_P               "Product stream"

  eta_init          "initial purge split ratio"
  F_R_init(alpha)   "Initial values for reactor mass flow rates"
  T_init            "initial reactor temperature"
  F_A_init          "initial flux of educt A"
;


***************load data created in MATLAB*********************************
$if NOT exist data.gdx  $abort "File data.gdx does not exist: did you run WilliamsOttoRun from Matlab to create it?"
$gdxin data
$load alpha m k_0 E_A rho F_P eta_init F_R_init T_init F_A_init
$gdxin

***************************************************************************
VARIABLES
  F_R(alpha)                "reactor mass fluxes"
  F_A                       "flux of educt A"
    
  !!decision variables
  F_B                       "flux of educt B"
  eta                       "purge split ratio"
  V                         "reactor volume"
  T                         "reaction temperature"

  !!help variables
  W(alpha)                  "mass fraction in CSTR"
  k(m)                      "reaction rate"
  F_purge                   "purge mass stream (fuel value)"
  F_recycle                 "recycle stream"

  !!objective function variables
  FCI                       "fixed capital cost ($)"
  C_Op                      "Operating Cost ($)"
  TAC                       "total annual cost ($)"
  PBT                       "profit before taxes ($/an)"
  CF                        "annual cash flow ($/an)"
  PBP                       "payback period (an)"
  NPW                       "net present worth value"
  z                         "variable for objective function"
;

POSITIVE VARIABLES  F_R, F_A, F_B, eta, V, T, W, k, F_purge, F_recycle;

EQUATIONS
  !!computation of help variables
  eq_W(alpha)       "computation of mass fractions"
  eq_k(m)           "computation of reaction rates"
  eq_F_P            "computation of product stream"
  eq_F_purge        "computation of purge stream"
  eq_F_recycle      "computation of recycle stream"

  !!Mass balances
  MB_A
  MB_B
  MB_C
  MB_E
  MB_G
  MB_P

  !!objective function(s)
  obj               "objective function"
  eq_FCI
  eq_C_Op
  eq_TAC
  eq_PBT
  eq_CF
  eq_PBP
  eq_NPW
;

!! a set defined for doing summation
SET alpha_temp(alpha) "used for summation issues" /A,B,C,E,G,P/;

***************computation of help variables*******************************
eq_W(alpha)..   W(alpha)    =e= F_R(alpha)/sum(alpha_temp,F_R(alpha_temp));
eq_k(m)..       k(m)        =e= k_0(m)*exp(-E_A(m)/1.8/T);
eq_F_P..        F_P         =e= F_R("P")-0.1*F_R("E");
eq_F_purge..    F_purge     =e= eta*(sum(alpha_temp,F_R(alpha_temp))-F_R("G")-F_P);
eq_F_recycle..  F_recycle   =e= (1-eta)*(sum(alpha_temp,F_R(alpha_temp))-F_R("G")-F_P);

***************Mass Balances***********************************************
MB_A..   0 =e= F_A+(1-eta)*F_R("A")-(k("1")*W("A")*W("B"))*V*rho-F_R("A");
MB_B..   0 =e= F_B+(1-eta)*F_R("B")-(k("1")*W("A")+k("2")*W("C"))*W("B")*V*rho-F_R("B");
MB_C..   0 =e= (1-eta)*F_R("C")+(2*k("1")*W("A")*W("B")-2*k("2")*W("B")*W("C")-k("3")*W("P")*W("C"))*V*rho-F_R("C");
MB_E..   0 =e= (1-eta)*F_R("E")+(2*k("2")*W("B")*W("C"))*V*rho-F_R("E");
MB_G..   0 =e= (1.5*k("3")*W("P")*W("C"))*V*rho-F_R("G");
MB_P..   0 =e= (k("2")*W("B")*W("C")-0.5*k("3")*W("P")*W("C"))*V*rho-0.1*eta*F_R("E")-F_P;

***************objective function(s)***************************************
eq_FCI..    FCI  =e= 600*V*rho/0.453;
eq_C_Op..   C_Op =e= 1/0.453 * (168*F_A + 252*F_B + 2.22*(F_recycle + F_A + F_B) + 0.84*F_R("G")) + 1041.6;
eq_TAC..    TAC  =e= C_Op + FCI/10;
eq_PBT..    PBT  =e= 1/0.453 * (2207*F_P + 50*F_purge) - TAC;
eq_CF..     CF   =e= 0.7*(1/0.453*(2207*F_P + 50*F_purge - 168*F_A - 252*F_B - 2.22*(F_recycle + F_A + F_B) - 84*F_R("G")) - 1041.6) + 0.3*(60*V*rho/0.453);
eq_PBP..    PBP  =e= FCI/CF;
eq_NPW..    NPW  =e= -FCI + 5.65022*CF; !!f_PA=((1+i)^10-1)/(0.12(1+0.12)^10)=5.65022 with expected rate of return i=0.12
obj..       z    =e= PBP;

***************box constraints*********************************************
T.lo        = 322.0;
T.up        = 378.0;
eta.lo      = 0.0;
eta.up      = 0.99;
V.lo        = 0.85;
V.up        = 10;
F_B.lo      = 10000;
F_B.up      = 15000;
F_A.lo      = 1000;
F_A.up      = 15000;

***************Initial Values**********************************************
F_R.l(alpha)    = F_R_init(alpha);
T.l             = T_init;
eta.l           = eta_init;
F_A.l           = F_A_init;
CF.l            = 2.5e6;



***************MODEL and SOLVE statements**********************************
MODEL WilliamsOttoProcess /all/;
SOLVE WilliamsOttoProcess using nlp minimizing z;

***************Unload Optimization Results*********************************
scalars modelStat, solveStat, zLevel, etaLevel, VLevel, TLevel,
        F_BLevel, F_ALevel, F_purgeLevel,
        FCILevel, C_OpLevel, TACLevel, PBTLevel, CFLevel, PBPLevel, NPWLevel;
parameter F_RLevel(alpha);

modelStat = WilliamsOttoProcess.modelstat;
solveStat = WilliamsOttoProcess.solvestat;
zLevel = z.l;
etaLevel = eta.l;
VLevel = V.l;
TLevel = T.l;
F_BLevel = F_B.l;
F_ALevel = F_A.l;
F_purgeLevel = F_purge.l;
F_RLevel(alpha) = F_R.l(alpha);
FCILevel = FCI.l;
C_OpLevel = C_Op.l;
TACLevel = TAC.l;
PBTLevel = PBT.l;
CFLevel = CF.l;
PBPLevel = PBP.l;
NPWLevel = NPW.l;

execute_unload 'solution', modelStat, solveStat, zLevel, etaLevel,
                VLevel, TLevel, F_BLevel, F_ALevel, F_purgeLevel, F_RLevel, FCILevel,
                C_OpLevel, TACLevel, PBTLevel, CFLevel, PBPLevel, NPWLevel;